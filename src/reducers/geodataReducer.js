import { fromJS, List, Map } from "immutable";
import geodata from '../models/geodata';
import * as ActionTypes from "../constants/actionTypes";
import moment from 'moment-timezone';


export const geodataReducer = (state = geodata(), action = {}) => {
  state = fromJS(state);

  switch (action.type) {
    case ActionTypes.SET_GEODATA:
      return state.update("geodata", Map(), capitals =>
        capitals.update(action.payload.name, Map(), capital =>
          capital
            .set("latitude", action.payload.value.results[0].geometry.location.lat)
            .set("longitude", action.payload.value.results[0].geometry.location.lng)
            .set("city", List(action.payload.value.results[0].formatted_address.split(',')).first().trim())
            .set("country", List(action.payload.value.results[0].formatted_address.split(',')).last().trim())
        )
      )
    case ActionTypes.SET_WEATHER:
      return state.update("geodata", Map(), capitals =>
        capitals.update(action.payload.name, Map(), capital =>
          capital
            .set("temperature", action.payload.value.currently.temperature)
            .set("time", moment.unix(action.payload.value.currently.time).tz(action.payload.value.timezone).format("HH:mm:ss a"))
            .set("timezone", action.payload.value.timezone)
        )
      )
    case ActionTypes.SET_ERROR_NESTED_NAME_LEVEL_ONE:
      return state.update("geodata", List(), capitals =>
        capitals.update(action.payload.name, Map(), capital =>
          capital.set(action.payload.nestedNameLevelOne, action.payload.value)
        )
      )
    default:
      return state;
  }
};