import { fromJS, List } from "immutable";
import cities from '../models/cities';
import * as ActionTypes from "../constants/actionTypes";

export const citiesReducer = (state = cities(), action = {}) => {
  state = fromJS(state);

  switch (action.type) {
    case ActionTypes.SET_SELECTOR_CITIES_LIST:
      return state.mergeWith(
        (prev, next) => next,
        fromJS({
          list: action.payload.value,
          loadedAt: new Date(),
        })
      );
    case ActionTypes.UNSET_SELECTOR_CITIES_LIST:
      return state.mergeWith(
        (prev, next) => next,
        fromJS({
          list: List(),
          loadedAt: undefined,
        })
      );
    default:
      return state;
  }
};