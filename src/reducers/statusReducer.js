import { fromJS } from "immutable";
import status from '../models/status';
import * as ActionTypes from "../constants/actionTypes";

export const statusReducer = (state = status(), action = {}) => {
  state = fromJS(state);

  switch (action.type) {
    case ActionTypes.CHANGE_STATE:
      return state.set(action.payload.name, action.payload.value);
    case ActionTypes.SET_SNACKBAR_ENQUEUER:
      if (state.get('enqueueSnackbar') !== undefined) {
        throw new Error('No se puede configurar un enqueueSnackbar si existe uno ya definido');
      }
      return state.set('enqueueSnackbar', action.payload.enqueueSnackbar);
    case ActionTypes.ENQUEUE_NOTIFICATION:
      state.get('enqueueSnackbar')(action.payload.message, {
        variant: action.payload.variant || 'default',
      });
      return state;
    default:
      return state;
  }
};