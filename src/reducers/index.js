import * as ContextTypes from '../constants/contexts';
import { fromJS } from "immutable";
import Model from '../models';
import { citiesReducer } from './citiesReducer';
import { statusReducer } from './statusReducer';
import { geodataReducer } from './geodataReducer';

export default (state = fromJS(Model()), action = {}) => {
  const context = action.payload != null ? action.payload.context : ContextTypes.UNKNOWN_CONTEXT;

  switch(context){
    case ContextTypes.CITIES:
      return state.update(ContextTypes.CITIES, cities => citiesReducer(cities, action))
    case ContextTypes.GEODATA:
      return state.update(ContextTypes.GEODATA, geodata => geodataReducer(geodata, action))
    case ContextTypes.APP_STATUS:
      return state.update(ContextTypes.APP_STATUS, status => statusReducer(status, action))
    case ContextTypes.UNKNOWN_CONTEXT:
      return state;
    default:
      return state;
  }
}