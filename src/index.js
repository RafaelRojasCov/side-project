import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import mainReducer from './reducers';
import Home from './components/Home';
import * as serviceWorker from './serviceWorker';

import './styles.css';

const logger = createLogger({collapsed: true});

const store = createStore(mainReducer, applyMiddleware(thunk, logger));

window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;

const mainApp = (
  <Provider store={store}>
    <SnackbarProvider maxSnack={4}>
      <Home />
    </SnackbarProvider>
  </Provider>
)

ReactDOM.render(mainApp, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
