const localProxyServer = 'https://acidlabs-app-proxy.herokuapp.com'


export default {
  urls: {
    capitals: `${localProxyServer}/capitals/`,
  },
  buildUrl: {
    geodata: (address) => `${localProxyServer}/geodata/${address}/`,
    weather: (lat, lng) => `${localProxyServer}/weather/${lat}/${lng}/`
  }
};
