import * as Validate from '../utils/validations';

export const getFromApi = (url, onSuccess, onError = () => {}, onPending = () => {}, ) => {
  /**
   * Validate if necessary params are ok or no, and then send the Pending Request.
   */
  Validate.getFromApiParams(url, onSuccess)
  onPending();

  return fetch(
    url, 
    { 
      method: "GET",
      redirect: "error"
    }
  )
    .then(response => {
      return response.json().then(data => {
        if(response.ok && data.status !== 'NOT-OK'){
          return onSuccess(data);
        } else {
          return onError(data);
        }
      })
    })
    .catch(error => {
      return onError(error)
    })
};

export const postToApi = (
  url,
  onSuccess, 
  onError = () => {},
  onPending = () => {},
  body = {}
) => {
  /**
   * Validate if necessary params are ok or no, and then send the Pending Request.
   */
  Validate.getFromApiParams(url, onSuccess);
  onPending();

  let headers = new Headers({
    "Content-Type": "application/json; charset=utf-8",
  });

  fetch(
    url,
    { 
      method: "POST",
      headers: headers,
      body: JSON.stringify(body),
    }
  )
    .then(response => {
      return response.json().then( data => {
        if( response.ok ){
          return onSuccess(data);
        } else {
          return onError(data);
        }
      })
    })
    .catch(error => onError(error));
}
