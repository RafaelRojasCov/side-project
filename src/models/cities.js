export default function(
  list = [],
  loadedAt = undefined
) {
  return {
    list,
    loadedAt
  }
};