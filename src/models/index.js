import cities from './cities';
import status from './status';
import * as ContextTypes from '../constants/contexts';
import geodata from './geodata';

export default () => ({
  [ContextTypes.CITIES]: cities(),
  [ContextTypes.GEODATA]: geodata(),
  [ContextTypes.APP_STATUS]: status(),
})