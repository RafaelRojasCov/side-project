import * as Statuses from '../constants/statuses';

export default (
  state = Statuses.IDLE,
  enqueueSnackbar = undefined
) => ({
  state,
  enqueueSnackbar
})