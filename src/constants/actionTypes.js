/**
 * Redux Store Actions
 */
export const FILL_FIELD_MODEL = 'FILL_FIELD_MODEL';

/**
 * App Status context specific
 */
export const CHANGE_STATE = 'CHANGE_STATE';
export const SET_ERROR = 'SET_ERROR';
export const SET_ERROR_NESTED_NAME_LEVEL_ONE = 'SET_ERROR_NESTED_NAME_LEVEL_ONE';
export const ENQUEUE_NOTIFICATION = 'ENQUEUE_NOTIFICATION';
export const SET_SNACKBAR_ENQUEUER = 'SET_SNACKBAR_ENQUEUER';
export const CLEAN_STATE_FIELD = 'CLEAN_STATE_FIELD';
export const CLEAN_STATE_NESTED_NAME_LEVEL_ONE = 'CLEAN_STATE_NESTED_NAME_LEVEL_ONE';

/**
 * Geodata context specific
 */
export const SET_GEODATA = 'SET_GEODATA';
export const SET_WEATHER = 'SET_WEATHER';


/**
 * Cities context specific
 */
export const SET_SELECTOR_CITIES_LIST = 'SET_SELECTOR_CITIES_LIST';
export const UNSET_SELECTOR_CITIES_LIST = 'UNSET_SELECTOR_CITIES_LIST';