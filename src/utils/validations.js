/**
 * Function that checks if the params passed to the getFromApi are valid or no.
 * @param {string} url 
 * @param {function} onSuccess 
 */
export const getFromApiParams = (url, onSuccess) => {
  if (
    url == null ||
    url === "" ||
    onSuccess == null ||
    typeof onSuccess !== "function"
  ) {
    throw new Error("url and onSuccess can't be null or empty");
  }
}