import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { connect } from 'react-redux';
import { lazyRequestCitySelector, handleRequestCapitalGeodata } from '../actions';
import * as ContextTypes from '../constants/contexts';
import CapitalInfo from './CapitalInfo';

const styles = (theme) => ({
  paper: {
    minHeight: '250px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formControl: {
    margin: theme.spacing.unit,
    width: "90%",
  },
})

const mapStateToProps = state => ({
  loadedAt: state.getIn([ContextTypes.CITIES, "loadedAt"]),
  cities: state.getIn([ContextTypes.CITIES, "list"]).sortBy((city) => city.get('capital')),
})

const mapDispatchToProps = dispatch => ({
  onMount: () => dispatch(lazyRequestCitySelector()),
  requestCapitalGeodata: (selectedCapital) => dispatch(handleRequestCapitalGeodata(selectedCapital)),
})

class Card extends React.PureComponent {
  constructor(props){
    super(props);
    this.state = {
      selectedCapital: props.defaultCapital
    }
  }

  componentDidMount(){
    setInterval(() => {
      this.props.requestCapitalGeodata(this.state.selectedCapital)  
    }, 60000)
  }


  handleChange = (selectedCapital) => {
    this.setState({ selectedCapital: selectedCapital},
      () => this.props.requestCapitalGeodata(selectedCapital)  
    );
  }
  
  render(){
    const { classes, name, id, cities, defaultCapital } = this.props;
    return (
      <Grid item xs={12} sm={6} md={4} className={classes.root} >
        <Paper className={classes.paper}>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor={name}>Seleccionar capital</InputLabel>
            <Select
              value={this.state.selectedCapital}
              onChange={e => this.handleChange(e.target.value)}
              inputProps={{
                name: `${name}`,
                id: `${id}`,
              }}
            >
            {cities && (cities.map((city, index) => {
              return (city.get("capital") !== "") && (
                  <MenuItem key={index} value={city.get("capital")}>{city.get("capital")}</MenuItem>
                )
            }))}
            </Select>
          </FormControl>
  
          <CapitalInfo selectedCapital={this.state.selectedCapital} defaultCapital={defaultCapital} />
        </Paper>
      </Grid>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Card));
