import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

const CapitalInfoItem = ({ icon, primaryText, secondaryText }) => {
  return (
    <ListItem>
      <ListItemIcon>
        {icon}
      </ListItemIcon>
      <ListItemText
        primary={primaryText}
        secondary={secondaryText}
      />
    </ListItem>
  )
}

export default CapitalInfoItem;