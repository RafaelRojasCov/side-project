import React, { useEffect, Fragment } from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { lazyRequestCitySelector } from '../actions';
import { connect } from 'react-redux';
import Card from './Card';
import Notifier from './Notifier';

const styles = () => ({
  title: {
    color: '#39B54A',
    fontWeight: 'strong',
    width: '98%',
    textAlign:'center',
    margin: '1em auto',
  }
});

const mapStateToProps = (state) => ({})

const mapDispatchToProps = (dispatch) => ({
  onMount: () => {
    return dispatch(lazyRequestCitySelector())
  },
})

const Home = ({ classes, onMount }) => {

  useEffect(() => {
    onMount();
  },[])

  return (
    <Fragment>
      <Notifier />
      <Grid container >
        <Typography
          variant="h2"
          className={classes.title} 
          gutterBottom
        >
          Desafío Acid Labs
        </Typography>
      </Grid>
      <Grid container justify="space-around" spacing={8}>
        
        <Card 
          id="first-capital-selector"
          name="first-capital-selector"
          defaultCapital="London"
        />
        <Card 
          id="second-capital-selector"
          name="second-capital-selector"
          defaultCapital="Washington, D.C."
        />
        <Card 
          id="third-capital-selector"
          name="third-capital-selector"
          defaultCapital="Buenos Aires"
        />

      </Grid>
    </Fragment>
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Home));