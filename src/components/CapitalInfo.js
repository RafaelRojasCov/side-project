import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import * as ContextTypes from '../constants/contexts';
import List from '@material-ui/core/List';
import ScheduleIcon from '@material-ui/icons/Schedule';
import WbSunnyIcon from '@material-ui/icons/WbSunny';
import MyLocationIcon from '@material-ui/icons/MyLocation';
import PublicIcon from '@material-ui/icons/Public'
import LocationOnIcon from '@material-ui/icons/LocationOn';
import CapitalInfoItem from './CapitalInfoItem';
import { handleRequestCapitalGeodata, handleRequestCapitalWeather } from '../actions';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginBottom: '12px',
    width: '95%',
  },
  list: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing.unit,
  },
});

const mapStateToProps = (state, ownProps) => ({
  latitude: state.getIn([ContextTypes.GEODATA, "geodata", ownProps.selectedCapital, "latitude"]),
  longitude: state.getIn([ContextTypes.GEODATA, "geodata", ownProps.selectedCapital, "longitude"]),
  country: state.getIn([ContextTypes.GEODATA, "geodata", ownProps.selectedCapital, "country"]),
  city: state.getIn([ContextTypes.GEODATA, "geodata", ownProps.selectedCapital, "city"]),
  temperature: state.getIn([ContextTypes.GEODATA, "geodata", ownProps.selectedCapital, "temperature"]),
  time: state.getIn([ContextTypes.GEODATA, "geodata", ownProps.selectedCapital, "time"]),
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  onMount: () => dispatch(handleRequestCapitalGeodata(ownProps.defaultCapital)),
})

const CapitalInfo = ({ onMount, classes, latitude, longitude, country, city, temperature, time, defaultCapital, selectedCapital }) => {

  useEffect(() => {
    onMount();
  },[])

  useEffect(() => {
    if (latitude != null && longitude != null){
      handleRequestCapitalWeather((selectedCapital || defaultCapital), latitude, longitude)
    }
  })

  return (
    <div className={classes.root}>
      <List dense>
        <CapitalInfoItem
          icon={<PublicIcon />}
          primaryText={latitude}
          secondaryText="Latitud"
        />
        <CapitalInfoItem
          icon={<PublicIcon />}
          primaryText={longitude}
          secondaryText="Longitud"
        />
        <CapitalInfoItem
          icon={<LocationOnIcon />}
          primaryText={country}
          secondaryText="País"
        />
        <CapitalInfoItem
          icon={<MyLocationIcon />}
          primaryText={city}
          secondaryText="Ciudad"
        />
        <CapitalInfoItem
          icon={<ScheduleIcon />}
          primaryText={time}
          secondaryText="Hora"
        />
        <CapitalInfoItem
          icon={<WbSunnyIcon />}
          primaryText={`${temperature || ""}º`}
          secondaryText="Temperatura"
        />
      </List>
    </div>
  )
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(CapitalInfo));
