import * as ActionTypes from '../constants/actionTypes';
import * as ContextTypes from '../constants/contexts';
/**
 * Creates the action for changing the current state of a request
 */
export const changeState = (context = ContextTypes.APP_STATUS) => (name, value) => ({
	type: ActionTypes.CHANGE_STATE,
	payload: { context, name, value }
});

export const enqueueNotification = (context = ContextTypes.APP_STATUS) => (message, variant) => ({
	type: ActionTypes.ENQUEUE_NOTIFICATION,
	payload: { context , message, variant },
});

export const setSnackbarEnqueuer = (context = ContextTypes.APP_STATUS) => (enqueueSnackbar) => ({
  type: ActionTypes.SET_SNACKBAR_ENQUEUER,
  payload: { context, enqueueSnackbar },
});

export const cleanStateField = (context = ContextTypes.APP_STATUS) => (name, value) => ({
	type: ActionTypes.CLEAN_STATE_FIELD,
	payload: { context, name, value }
})

export const cleanStateNestedNameLevelOne = (context = ContextTypes.APP_STATUS) => (name, nestedNameLevelOne, value) => ({
	type: ActionTypes.CLEAN_STATE_NESTED_NAME_LEVEL_ONE,
	payload: { context, name, nestedNameLevelOne, value }
})

export const setError = (context = ContextTypes.APP_STATUS) => (name, value) => ({
	type: ActionTypes.SET_ERROR,
	payload: { context, name, value }
})

export const setErrorNestedNameLevelOne = (context = ContextTypes.APP_STATUS) => (name, nestedNameLevelOne, value) => ({
	type: ActionTypes.SET_ERROR_NESTED_NAME_LEVEL_ONE,
	payload: { context, name, nestedNameLevelOne, value }
})