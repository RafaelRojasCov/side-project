import * as ActionTypes from '../constants/actionTypes';
import * as Statuses from '../constants/statuses';
import * as ContextTypes from '../constants/contexts';
import { 
  changeState, 
  enqueueNotification, 
  setErrorNestedNameLevelOne,
  cleanStateNestedNameLevelOne
  } from './index';
import { getCapitalGeoData, getCapitalWeather } from '../services';

const setCapitalData = (context, type, name, value) => ({
  type,
  payload: { context, name, value }
})

const onSuccess = (dispatch, context, type, name) => response => new Promise(resolve => {
  try {
    dispatch(changeState()("state", Statuses.SUCCESS));
    dispatch(setCapitalData(context, type, name, response.data));
    dispatch(cleanStateNestedNameLevelOne(context)(name, 'error', undefined));
    resolve(response);
  } catch (error) {
    console.warn(`try/catch onSuccess (${error})`)
  }
})

const onError = (dispatch, context, name, nestedNameLevelOne) => response => new Promise(resolve => {
  try {
    dispatch(changeState()("state", Statuses.ERROR));
    dispatch(setErrorNestedNameLevelOne(context)(name, nestedNameLevelOne, (
      (response.status === 'NOT-OK' && response.message) || response))
    )
    if (response.status === 'NOT-OK'){
      resolve(response)
    } else {
      dispatch(enqueueNotification()(response.message, 'error'))
    }
  } catch (error) {
    console.warn(`try/catch onError (${error})`)
  }
})

const onPending = (dispatch) => () => {
  try {
    dispatch(changeState()("state", Statuses.PENDING));
  } catch (error) {
    console.warn(`try/catch onPending (${error})`);
  }
};

export const handleRequestCapitalGeodata = (selectedCapital) => (dispatch, getState) => {
  try {
    return dispatch(getCapitalGeoData(
      selectedCapital,
      onSuccess(dispatch, ContextTypes.GEODATA, ActionTypes.SET_GEODATA, selectedCapital),
      onError(dispatch, ContextTypes.GEODATA, selectedCapital, "error"),
      onPending(dispatch)
    )).then((response) => {
      if (response.status === 'NOT-OK' && response.message === 'How unfortunate! The API Request Failed'){
        return dispatch(handleRequestCapitalGeodata(selectedCapital));
      } else if (response.status === 'NOT-OK'){
        dispatch(enqueueNotification()(response.message, 'error'))
      } else {
        const lat = getState().getIn([ContextTypes.GEODATA, "geodata", selectedCapital, "latitude"]);
        const lng = getState().getIn([ContextTypes.GEODATA, "geodata", selectedCapital, "longitude"]);
        return dispatch(handleRequestCapitalWeather(selectedCapital, lat, lng));
      }
    })
  } catch (error) {
    console.warn(`try/catch handleRequestCapitalGeodata (${error})`)
  }
};

export const handleRequestCapitalWeather = (selectedCapital, latitude, longitude) => (dispatch, getState) => {
  try {
    dispatch(getCapitalWeather(
      latitude,
      longitude,
      onSuccess(dispatch, ContextTypes.GEODATA, ActionTypes.SET_WEATHER, selectedCapital),
      onError(dispatch, ContextTypes.GEODATA, selectedCapital, "error"),
      onPending(dispatch)
    )).then((response) => {
      if (response.status === 'NOT-OK' && response.message === 'How unfortunate! The API Request Failed'){
        return dispatch(handleRequestCapitalWeather(selectedCapital, latitude, longitude));
      } else if (response.status === 'NOT-OK'){
        return dispatch(enqueueNotification()(response.message, 'error'))
      }
    })
  } catch (error) {
    console.warn(`try/catch handleRequestCapitalWeather (${error})`)
  }
};