import * as ActionTypes from '../constants/actionTypes';
import * as Statuses from '../constants/statuses';
import * as ContextTypes from '../constants/contexts';
import { changeState, enqueueNotification } from './index';
import { getCityList } from '../services';

const setCityList = (context, name, value) => ({
  type: ActionTypes.SET_SELECTOR_CITIES_LIST,
  payload: { context, name, value },
});

const unSetCityList = (context, name) => ({
  type: ActionTypes.UNSET_SELECTOR_CITIES_LIST,
  payload: { context, name },
});

const shouldReload = (getState, name) => {
  const loadedAt = getState().getIn([name, "loadedAt"]);
  const minimalReloadTime = 600000; // 1 hour to reload again requested lists

  return loadedAt == null ||
    new Date().getTime() - loadedAt.getTime() > minimalReloadTime
}

const onSuccess = (dispatch, context, name) => response => new Promise(resolve => {
  try {
    dispatch(changeState()("state", Statuses.SUCCESS));
    dispatch(setCityList(context, name, response.data));
    resolve(response)
  } catch (error) {
    console.warn(`try/catch onSuccess (${error})`)
  }
})

const onError = (dispatch, context, name) => response => new Promise(resolve => {
  try {
    dispatch(changeState()("state", Statuses.ERROR));
    dispatch(unSetCityList(context, name));
    if (response.status === 'NOT-OK'){
      resolve(response)
    } else {
      dispatch(enqueueNotification()(response.message, 'error'))
    }
  } catch (error) {
    console.warn(`try/catch onError (${error})`)
  }
})

const onPending = (dispatch, context, name) => () => {
  try {
    dispatch(changeState()("state", Statuses.PENDING));
    dispatch(unSetCityList(context, name));
  } catch (error) {
    console.warn(`try/catch onPending (${error})`);
  }
};

export const lazyRequestCitySelector = () => (dispatch, getState) => {
  if (shouldReload(getState, ContextTypes.CITIES)){
    return dispatch(getCityList(
      onSuccess(dispatch, ContextTypes.CITIES, "list"),
      onError(dispatch, ContextTypes.CITIES, "list"),
      onPending(dispatch, ContextTypes.CITIES, "list")
    )).then((response) => {
      if (response.status === 'NOT-OK' && response.message === 'How unfortunate! The API Request Failed'){
        return dispatch(lazyRequestCitySelector());
      } else if (response.status === 'NOT-OK'){
        return dispatch(enqueueNotification()(response.message, 'error'))
      }
    })
    .catch((error) => {
      return dispatch(enqueueNotification()(error, 'error'))
    })
  }
}


export const handleUnSetCityList = () => (dispatch, getState) =>
  dispatch(unSetCityList(ContextTypes.CITIES, "list"));