import Settings from '../apis/Settings';
import { getFromApi } from '../apis/apiRequest';

export const getCityList = (onSuccess, onError, onPending) => () =>
  getFromApi(
    Settings.urls.capitals,
    onSuccess, 
    onError, 
    onPending
  )

export const getCapitalGeoData = (selectedCapital, onSuccess, onError, onPending) => () =>
  getFromApi(
    Settings.buildUrl.geodata(selectedCapital),
    onSuccess,
    onError,
    onPending
  )

export const getCapitalWeather = (latitude, longitude, onSuccess, onError, onPending) => () =>
  getFromApi(
    Settings.buildUrl.weather(latitude, longitude),
    onSuccess,
    onError,
    onPending
  )